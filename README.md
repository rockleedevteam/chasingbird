# README #

This README would normally document whatever steps are necessary to get your application up and running.

### How do I get set up? ###

* git clone https://rockleedev@bitbucket.org/rockleedevteam/chasingbird.git
* Open StrayBird.xcworkspace by Xcode

### Version History ###

* 0.1: Working version with Apple Watch, LEO and SensorTag
* 1.0: Remove support to Apple Watch and LEO. Import HockeyApp for analytics and distribution.